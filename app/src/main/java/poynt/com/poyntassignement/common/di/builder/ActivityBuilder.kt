package poynt.com.poyntassignement.common.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import poynt.com.poyntassignement.feature.activity.MapsActivity
import poynt.com.poyntassignement.feature.activity.MapsActivityModule

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [(MapsActivityModule::class)])
    internal abstract fun bindSplashActivity(): MapsActivity
}