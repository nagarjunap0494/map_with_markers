package poynt.com.poyntassignement.common.data.model.others

data class ApiErrorBody(var success: Boolean, var status:Int, var message: String) {
}