package poynt.com.poyntassignement.common.utils

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import poynt.com.poyntassignement.R
import poynt.com.poyntassignement.common.data.model.others.ApiErrorBody
import java.lang.Exception

class AppUtils {

    companion object {

        fun showLoadingDialog(context: Context): ProgressDialog {
            val progressDialog = ProgressDialog(context)
            progressDialog.show()
            if (progressDialog.window != null) {
                progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
            progressDialog.setContentView(R.layout.progress_dialog)
            progressDialog.isIndeterminate = true
            progressDialog.setCancelable(false)
            progressDialog.setCanceledOnTouchOutside(false)
            return progressDialog
        }

        fun isNetworkConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            if (cm != null) {
                val activeNetwork = cm.activeNetworkInfo
                return activeNetwork != null && activeNetwork.isConnectedOrConnecting
            }
            return false
        }

        fun parseErrorBody(errorBody: String): ApiErrorBody? {
            var apiError: ApiErrorBody? = null
            val gson = GsonBuilder().create()
            try {
                apiError = gson.fromJson(errorBody, ApiErrorBody::class.java)
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return apiError
        }
    }
}