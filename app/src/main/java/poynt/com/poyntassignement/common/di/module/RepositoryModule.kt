package poynt.com.poyntassignement.common.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import poynt.com.poyntassignement.common.data.api.ApiHelper
import poynt.com.poyntassignement.common.data.AppRepository
import poynt.com.poyntassignement.common.data.preference.AppPreference
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    internal fun provideAppRepository
            (mContext: Context, appPreference: AppPreference, appWebService: ApiHelper)
            : AppRepository {

        return AppRepository(mContext, appPreference, appWebService)
    }
}