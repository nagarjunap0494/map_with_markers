package poynt.com.poyntassignement.common.utils

class AppConstants {

    companion object {

        const val PREF_NAME = "poynt_pref"

        /*headers*/
        //Body
        const val HEADER_BODY_ACCEPT = "application/json"
        const val HEADER_BODY_CONTENT_TYPE = "application/json"

        //Keys
        const val HEADER_KEY_ACCEPT = "Accept"
        const val HEADER_KEY_CONTENT_TYPE = "Content-Type"
        const val HEADER_KEY_AUTHORIZATION = "Authorization"

        /*Network request time out*/
        const val NETWORK_REQUEST_TIMEOUT = 60L

        const val NETWORK_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}