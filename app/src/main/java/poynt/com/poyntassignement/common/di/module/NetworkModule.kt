package poynt.com.poyntassignement.common.di.module

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.CookieJar
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import poynt.com.poyntassignement.BuildConfig
import poynt.com.poyntassignement.common.data.api.ApiHelper
import poynt.com.poyntassignement.common.data.preference.AppPreference
import poynt.com.poyntassignement.common.utils.AppConstants
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideRetrofit(sharedPref: AppPreference): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            interceptor.level = HttpLoggingInterceptor.Level.NONE
        }

        val httpClient = OkHttpClient.Builder()
                .addInterceptor(Interceptor { chain ->
                    chain.proceed(chain.request().newBuilder()
                            .addHeader(AppConstants.HEADER_KEY_ACCEPT, AppConstants.HEADER_BODY_ACCEPT)
                            .addHeader(AppConstants.HEADER_KEY_CONTENT_TYPE, AppConstants.HEADER_BODY_CONTENT_TYPE)
                            //.addHeader(AppConstants.HEADER_KEY_AUTHORIZATION, sharedPref.getAccessToken()?:"")
                            .build())
                })
                .addInterceptor(interceptor)
                .cookieJar(CookieJar.NO_COOKIES)
                .connectTimeout(AppConstants.NETWORK_REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(AppConstants.NETWORK_REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(AppConstants.NETWORK_REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .build()

        val gson = GsonBuilder()
                .setDateFormat(AppConstants.NETWORK_DATE_FORMAT)
                .create()


        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Provides
    @Singleton
    internal fun provideAppApiService(retrofit: Retrofit): ApiHelper {
        return retrofit.create(ApiHelper::class.java)
    }
}