package poynt.com.poyntassignement.common.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import poynt.com.poyntassignement.common.App
import poynt.com.poyntassignement.common.di.builder.ActivityBuilder
import poynt.com.poyntassignement.common.di.module.AppModule
import poynt.com.poyntassignement.common.di.module.NetworkModule
import poynt.com.poyntassignement.common.di.module.RepositoryModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (AppModule::class),
    (NetworkModule::class), (RepositoryModule::class), (ActivityBuilder::class)])
interface AppComponent {

    fun inject(app: App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}