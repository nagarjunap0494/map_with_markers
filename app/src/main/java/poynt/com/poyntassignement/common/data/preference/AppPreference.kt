package poynt.com.poyntassignement.common.data.preference;

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPreference @Inject constructor(mContext: Context) : PreferencesHelper {

    /*Secure Shared Pref Class
   * which will be used to store
   * encrypted and decrypted data*/
    val mPrefs: SecurePreferences = SecurePreferences(mContext)

}