package poynt.com.poyntassignement.common.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import poynt.com.poyntassignement.common.data.preference.AppPreference
import poynt.com.poyntassignement.common.utils.RxSchedule
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    internal fun provideAppPreference(mContext: Context): AppPreference {
        return AppPreference(mContext)
    }

    @Provides
    internal fun provideRxSchedule(): RxSchedule {
        return RxSchedule()
    }
}