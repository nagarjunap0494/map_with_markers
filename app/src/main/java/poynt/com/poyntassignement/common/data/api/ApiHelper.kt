package poynt.com.poyntassignement.common.data.api

import io.reactivex.Single
import poynt.com.poyntassignement.common.data.model.api.response.ListResponseModel
import javax.inject.Singleton

import retrofit2.http.GET

@Singleton
interface ApiHelper {

    /*
    * Api key with place
    * */
    //@GET("https://api.openweathermap.org/data/2.5/weather?q=dhaka,bd&units=metric&appid=8118ed6ee68db2debfaaa5a44c832918")

    /*
    * Example Api key
    * */

    //@GET("https://api.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=8118ed6ee68db2debfaaa5a44c832918")
    //
    /*
    * My own API key
    * */
    @GET("https://api.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=8118ed6ee68db2debfaaa5a44c832918")
    fun getWeatherList(): Single<ListResponseModel>
}