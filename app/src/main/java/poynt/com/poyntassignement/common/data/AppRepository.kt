package poynt.com.poyntassignement.common.data;

import android.content.Context
import io.reactivex.Single
import poynt.com.poyntassignement.common.data.api.ApiHelper
import poynt.com.poyntassignement.common.data.model.api.response.ListResponseModel
import poynt.com.poyntassignement.common.data.preference.AppPreference
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepository
@Inject constructor(private val mContext: Context,
                    private val mAppPreference: AppPreference,
                    private val mAppApiService: ApiHelper) : AppRepositoryHelper {

    override fun getWeatherList(): Single<ListResponseModel> {
        return mAppApiService.getWeatherList()
    }
}