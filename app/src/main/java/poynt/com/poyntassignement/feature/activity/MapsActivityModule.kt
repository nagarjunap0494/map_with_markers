package poynt.com.poyntassignement.feature.activity

import dagger.Module
import dagger.Provides
import poynt.com.poyntassignement.common.data.AppRepository
import poynt.com.poyntassignement.common.utils.RxSchedule

@Module
class MapsActivityModule {

    @Provides
    internal fun provideMapsViewModel(appRepository: AppRepository, rxSchedule: RxSchedule): MapsViewModel {
        return MapsViewModel(appRepository, rxSchedule)
    }
}