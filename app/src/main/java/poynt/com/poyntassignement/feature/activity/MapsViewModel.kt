package poynt.com.poyntassignement.feature.activity

import poynt.com.poyntassignement.common.data.AppRepository
import poynt.com.poyntassignement.common.utils.RxSchedule
import poynt.com.poyntassignement.feature.base.BaseViewModel
import javax.inject.Inject

class MapsViewModel @Inject constructor(appRepository: AppRepository, rxSchedule: RxSchedule)
    : BaseViewModel<MapsNavigator>(appRepository, rxSchedule) {

      fun getWeatherList() {
        getNavigator().showLoading()
        mCompositeDisposable.add(mRepository.getWeatherList()
                .subscribeOn(mRxSchedule.io())
                .observeOn(mRxSchedule.ui())
                .subscribe({ response ->
                    if (response.cod == 200) {
                        getNavigator().hideLoading()
                        print("Success")
                        getNavigator().sendResponseBack(response)
                    } else {
                        getNavigator().hideLoading()
                        print("Success Error")
                    }

                }, { throwable ->
                    print("Success" + throwable)
                    getNavigator().hideLoading()
                    getNavigator().handleError(throwable)
                }))
    }
}