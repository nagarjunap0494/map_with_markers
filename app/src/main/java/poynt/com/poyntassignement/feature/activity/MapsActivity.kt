package poynt.com.poyntassignement.feature.activity

import android.content.Context
import android.os.Bundle
import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import poynt.com.poyntassignement.BR
import poynt.com.poyntassignement.R
import poynt.com.poyntassignement.common.data.model.api.response.ListResponseModel
import poynt.com.poyntassignement.databinding.ActivityMapsBinding
import poynt.com.poyntassignement.feature.base.BaseActivity
import javax.inject.Inject

class MapsActivity : BaseActivity<ActivityMapsBinding, MapsViewModel>(), MapsNavigator, OnMapReadyCallback {

    @Inject
    lateinit var mapsViewModel: MapsViewModel

    private lateinit var mapsBinding: ActivityMapsBinding
    private lateinit var mContext: Context

    override fun getBindingVariable(): Int = BR.viewModel
    override fun getLayoutId(): Int = R.layout.activity_maps
    override fun getViewModel(): MapsViewModel = mapsViewModel

    private var mGoogleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        mapsBinding = getViewDataBinding()
        mapsViewModel.setNavigator(this)
        setUp()
    }

    override fun setUp() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        mapsViewModel.getWeatherList()

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap

        val sydney = LatLng(-34.0, 151.0)
        mGoogleMap!!.addMarker(MarkerOptions().position(sydney).title("Marker"))
        mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun sendResponseBack(responseModel: ListResponseModel) {
        print(" ======> " + responseModel.list.size)

        for (i in 0 until responseModel.list.size) {
            println(i)

            val sydney = LatLng(responseModel.list[i].coord.Lat!!, responseModel.list[i].coord.Lon!!)
            mGoogleMap!!.addMarker(MarkerOptions().position(sydney).title(responseModel.list[i].name))
            mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        }
    }
}


