package poynt.com.poyntassignement.feature.activity

import poynt.com.poyntassignement.common.data.model.api.response.ListResponseModel

interface MapsNavigator {

    fun handleError(throwable: Throwable)

    fun showMessage(message: String)

    fun showLoading()

    fun hideLoading()

    fun sendResponseBack(response: ListResponseModel)
}