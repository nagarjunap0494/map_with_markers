package poynt.com.poyntassignement.feature.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import poynt.com.poyntassignement.common.data.AppRepository
import poynt.com.poyntassignement.common.utils.RxSchedule
import androidx.databinding.ObservableBoolean
import java.lang.ref.WeakReference

abstract class BaseViewModel<N>(appRepository: AppRepository, rxSchedule: RxSchedule) : ViewModel() {

    private lateinit var mNavigator: WeakReference<N>
    val mRepository = appRepository
    val mRxSchedule = rxSchedule
    val mCompositeDisposable = CompositeDisposable()
    var mIsLoading = ObservableBoolean(false)

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }

    fun getIsLoading(): ObservableBoolean {
        return mIsLoading
    }

    fun setIsLoading(isLoading: Boolean) {
        mIsLoading.set(isLoading)
    }

    fun getNavigator(): N = mNavigator.get()!!


    fun setNavigator(navigator: N) {
        this.mNavigator = WeakReference(navigator)
    }
}